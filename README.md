# assertx
#### [![Build Status]][actions] [![Latest Version]][crates.io] [![Docs]][docs.rs]
[Build Status]: https://gitlab.com/pfouilloux/assertx/badges/master/pipeline.svg
[actions]: https://gitlab.com/pfouilloux/assertx/-/pipelines
[Latest Version]: https://img.shields.io/crates/v/assertx.svg
[crates.io]: https://crates.io/crates/assertx
[Docs]: https://docs.rs/assertx/badge.svg
[docs.rs]: https://docs.rs/assertx

Additional assertion macros