#!/bin/bash

set -u
set -e

echo "Releasing $1"
if ! [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+(-rc[0-9]+)?$ ]]; then
    echo "Please provide a version number in SEMVER format."
    exit 1
fi

sed -re 's/^version = "0.0.0"/version = "'$1'"/' -i ./Cargo.toml
cat ./Cargo.toml