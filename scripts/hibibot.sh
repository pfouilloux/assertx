#!/bin/bash

set -u
set -e

cargo install cargo-edit
git remote set-url origin "$1"
git config --global user.email "hibibot@pm.me"
git config --global user.name "Hibibot"
git checkout -b "HIBIBOT-${CI_JOB_ID}"
git commit -am "$(cargo upgrade | grep Upgrading)"
git push -u origin "HIBIBOT-${CI_JOB_ID}"