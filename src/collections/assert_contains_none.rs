/// Asserts that the actual collection contains none of the elements in the expected collection
///
/// # Usage
/// ```
/// # #[macro_use] extern crate assertx;
/// # fn main() {
/// #   let actual = vec!["a"];
/// #   let expected = vec!["b"];
///     assert_contains_none!(actual, expected);
/// # }
/// ```
///
/// # Examples
///
/// Will succeed, actual collection does not contain "c"
/// ```
/// # #[macro_use] extern crate assertx;
/// # fn main() {
///     assert_contains_none!(vec!["a", "b"], vec!["c"]);
/// # }
/// ```
///
/// Will fail, actual collection contains "c"
/// ```should_panic
/// # #[macro_use] extern crate assertx;
/// # fn main() {
///     assert_contains_none!(vec!["a", "b", "c"], vec!["c"]);
/// # }
/// ```
#[macro_export]
macro_rules! assert_contains_none {
    ($actual: expr, $expected: expr) => {
        let actual = $actual.clone().into_iter();
        let expected = $expected.clone().into_iter();

        for e in expected.clone() {
            if actual.clone().any(|it| it == e) {
                panic!(
                    "Expected to find none of {:?} in order in {:?} - Found {:?}",
                    expected.as_slice(),
                    actual.as_slice(),
                    e
                )
            }
        }
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn should_succeed_if_both_sides_empty() {
        assert_contains_none!(Vec::<bool>::new(), Vec::<bool>::new());
    }

    #[test]
    fn should_succeed_if_no_shared_elements() {
        assert_contains_none!(vec![1, 2, 3], vec![4, 5, 6]);
    }

    #[test]
    #[should_panic(expected = r"Expected to find none of [4] in order in [2, 4, 6] - Found 4")]
    fn should_fail_if_any_shared_element() {
        assert_contains_none!(vec![2, 4, 6], vec![4]);
    }
}
