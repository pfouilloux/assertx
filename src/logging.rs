#[macro_use]
mod assert_logs_contain_in_order;

#[macro_use]
mod assert_logs_contain_none;

#[doc(hidden)]
pub mod interface;

mod log_inspector;
mod test_logger;
mod setup;

pub use setup::setup_logging_test;
