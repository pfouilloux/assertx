/// Asserts that logs contain none of the expected messages
/// All logging tests need to invoke the `logging::setup_logging_test` method to capture logs for assertion
///
/// # Examples
/// ```
/// # #[macro_use] extern crate assertx;
/// # #[macro_use] extern crate log;
/// # use assertx::logging;
/// # use log::Level;
/// # fn main() {
///     let logs = logging::setup_logging_test();
///
///     info!("Doing stuff");
///     warn!("Oops, something happened!");
///
///     assert_logs_contain_none!(
///         logs,
///         Level::Info => "Nothing to see here!"
///     );
/// # }
/// ```
#[macro_export]
macro_rules! assert_logs_contain_none {
    ($inspector: expr, $($level:expr => $line: expr),*) => {
        let mut expected = Vec::<$crate::logging::interface::LogLine>::new();
        $(
            expected.push($crate::logging::interface::LogLine($level, $line.to_string()));
        )*

        $inspector.assert_logs_contain_none(&expected);
    };
}

#[cfg(test)]
mod tests {
    use crate::logging::setup_logging_test;
    use log::{info, warn, Level};

    #[test]
    fn should_succeed_if_logs_contain_none_of_the_expected_items() {
        let logs = setup_logging_test();

        info!("Doing stuff");
        warn!("Oops, something happened!");

        assert_logs_contain_none!(
            logs,
            Level::Info => "Nothing to see here!"
        );
    }

    #[test]
    fn should_allow_expressions() {
        let logs = setup_logging_test();

        info!("Doing stuff");
        warn!("Oops, something happened!");

        assert_logs_contain_none!(
            logs,
            Level::Warn => format!("{}", "Nothing to see here!")
        );
    }
}
