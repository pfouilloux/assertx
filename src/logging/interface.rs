use log::Level;

pub trait LogInspector {
    #[deprecated(since = "1.1.2", note = "Please use assert_logs_contain_in_order instead, this method will be removed in a future version")]
    fn assert_logs_contain(&self, expected: &Vec<LogLine>) {
        self.assert_logs_contain_in_order(expected)
    }

    fn assert_logs_contain_in_order(&self, expected: &Vec<LogLine>);
    fn assert_logs_contain_none(&self, expected: &Vec<LogLine>);
}

#[derive(Clone, Debug, PartialEq)]
pub struct LogLine(pub Level, pub String);
