/// Deprecated. Please use [assert_logs_contain_in_order] instead
#[macro_export]
#[deprecated]
macro_rules! assert_logs_contain {
    ($inspector: expr, $($level:expr => $line: literal),*) => {
        let mut expected = Vec::<$crate::logging::interface::LogLine>::new();
        $(
            expected.push($crate::logging::interface::LogLine($level, $line.to_string()));
        )*

        $inspector.assert_logs_contain_in_order(&expected);
    };
}

/// Asserts that logs contain the expected messages
/// All logging tests need to invoke the `logging::setup_logging_test` method to capture logs for assertion
///
/// # Examples
/// ```
/// # #[macro_use] extern crate assertx;
/// # #[macro_use] extern crate log;
/// # use assertx::logging;
/// # use log::Level;
/// # fn main() {
///     let logs = logging::setup_logging_test();
///
///     info!("Doing stuff");
///     warn!("Oops, something happened!");
///
///     assert_logs_contain_in_order!(
///         logs,
///         Level::Info => "Doing stuff",
///         Level::Warn => "Oops, something happened!"
///     );
/// # }
/// ```
#[macro_export]
macro_rules! assert_logs_contain_in_order {
    ($inspector: expr, $($level:expr => $line: expr),*) => {
        let mut expected = Vec::<$crate::logging::interface::LogLine>::new();
        $(
            expected.push($crate::logging::interface::LogLine($level, $line.to_string()));
        )*

        $inspector.assert_logs_contain_in_order(&expected);
    };
}

#[cfg(test)]
mod tests {
    use crate::logging::setup_logging_test;
    use log::{info, warn, Level};

    #[test]
    fn should_succeed_if_logs_contain_all_of_the_expected_items_in_order() {
        let logs = setup_logging_test();

        info!("Doing stuff");
        warn!("Oops, something happened!");

        assert_logs_contain_in_order!(
            logs,
            Level::Warn => "Oops, something happened!"
        );
    }

    #[test]
    fn should_allow_expressions() {
        let logs = setup_logging_test();

        warn!("Oops, something happened!");

        assert_logs_contain_in_order!(
            logs,
            Level::Warn => format!("{}", "Oops, something happened!")
        );
    }
}
