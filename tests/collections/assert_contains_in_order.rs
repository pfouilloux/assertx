use assertx::assert_contains_in_order;

fn main() {
    assert_contains_in_order!(vec![1, 2, 3, 4], vec![2, 3]);
}