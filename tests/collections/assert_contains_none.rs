use assertx::assert_contains_none;

fn main() {
    assert_contains_none!(vec![1, 2, 3], vec![4]);
}