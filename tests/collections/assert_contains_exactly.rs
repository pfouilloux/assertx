use assertx::assert_contains_exactly;

fn main() {
    assert_contains_exactly!(vec![1, 2, 3, 4], vec![1, 2, 3, 4]);
}