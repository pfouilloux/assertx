use assertx::assert_same_variant;

#[derive(Debug)]
enum TestEnum {
    A,
}

fn main() {
    assert_same_variant!(TestEnum::A, TestEnum::A);
}
