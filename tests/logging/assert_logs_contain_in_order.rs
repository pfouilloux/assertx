use assertx::assert_logs_contain_in_order;
use log::{debug, error, info, trace, warn, Level};

fn main() {
    let logs = assertx::setup_logging_test();

    trace!("Trace");
    debug!("Debug");
    info!("Info");
    warn!("Warn");
    error!("Error");

    assert_logs_contain_in_order!(
        logs,
        Level::Trace => "Trace",
        Level::Debug => "Debug",
        Level::Info => "Info",
        Level::Warn => "Warn",
        Level::Error => "Error"
    );
}
