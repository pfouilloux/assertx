use assertx::assert_logs_contain_none;
use log::Level;

fn main() {
    let logs = assertx::setup_logging_test();

    assert_logs_contain_none!(logs, Level::Info => "Something");
}
