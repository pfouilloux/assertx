#[cfg(not(any(feature = "logging")))]
#[test]
fn when_no_features_are_enabled() {
    let t = trybuild::TestCases::new();
    t.pass("tests/collections/*.rs");
    t.compile_fail("tests/logging/*.rs");
    t.pass("tests/variants/*.rs")
}

#[cfg(feature = "logging")]
#[test]
fn when_logging_is_enabled() {
    let t = trybuild::TestCases::new();
    t.pass("tests/collections/*.rs");
    t.pass("tests/logging/*.rs");
    t.pass("tests/variants/*.rs")
}
