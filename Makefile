.PHONY: clean
clean:
	cargo clean

.PHONY: build
build:
	cargo build --verbose

.PHONY: vanilla-test
vanilla-test:
	cargo test --verbose

.PHONY: all-feature-test
all-feature-test:
	cargo test --all-features --verbose

.PHONY: test
test: vanilla-test all-feature-test

.PHONY: docs
docs:
	cargo doc --all-features --verbose